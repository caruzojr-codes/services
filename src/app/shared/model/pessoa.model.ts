export class Pessoa {

	constructor(
		public id: number,
		public nome: string,
		public idade: number,
		public sexo: string,
		public email: string
  ){}

}


// export class Pessoa2 {
//   id: number;
//   nome: string;
//   idade: number;
//   sexo: string;
//   email: string;

//   constructor({ nome, idade, sexo, email }: Omit<Pessoa2, 'id'>) {
//     this.id = 123456;
//     this.nome = nome;
//     this.idade = idade;
//     this.sexo = sexo;
//     this.email = email;
//   }
// }
