import { Injectable } from '@angular/core';

import { Pessoa } from '../model/pessoa.model';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  recuperarPessoaLista(): Array<Pessoa> {

    let listaPessoa: Pessoa[] = [];

    listaPessoa.push(new Pessoa(1, 'Caruzo Jr', 34, 'Masculino', 'caruzojr@udemy.com'));
    listaPessoa.push(new Pessoa(2, 'Jorge Caruzo', 30, 'Masculino', 'jorge@udemy.com'));

    return listaPessoa;

  }

  constructor() { }

}
