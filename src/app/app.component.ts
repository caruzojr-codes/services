import { Component, OnInit } from '@angular/core';

import { PessoaService } from './shared/services/pessoa.service';
import { Pessoa } from './shared/model/pessoa.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public pessoas: Pessoa[] = [];


  constructor (
    private PessoaServico: PessoaService
  ) { }

  ngOnInit() {
    this.pessoas = this.PessoaServico.recuperarPessoaLista();

    console.log(this.pessoas);
  }

}
