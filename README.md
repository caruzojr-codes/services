# Basic Project
Como trabalhar com services responsável por realizar a manipulação dos dados quem vem da API.



## Run Project
Clone este repositório para a máquina local
```
git clone https://caruzojr@bitbucket.org/caruzojr/basic-project.git
```
Dentro da pasta do projeto, execute o comando abaixo para baixar todas as dependencias necessárias
```
npm install
```
Rode o seguinte comando para executar o projeto
```
ng s --open
```
